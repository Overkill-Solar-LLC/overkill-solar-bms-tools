These are the saved parameter files for each version of the Overkill Solar BMSs as of 6-8-2021.
Use the desktop app to load the files, then write to the BMS.

These files are compatable with:

Overkill-Solar-bms-tools https://gitlab.com/Overkill-Solar-LLC/overkill-solar-bms-tools/

BMS-Tools https://gitlab.com/bms-tools/bms-tools

JBD-Tools https://github.com/FurTrader/OverkillSolarBMS/raw/master/Desktop%20app%20with%20password%20reset-%20JBDTools%20V2.9-20210524.zip